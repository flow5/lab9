import src/rpn/rpn;

export {
    evaluate(rpnExpression : [RpnToken], variableValues : Tree<string, double>) -> Maybe<double>;
}

evaluate(rpnExpression : [RpnToken], variableValues : Tree<string, double>) -> Maybe<double> {
    applyOp : (RpnOperation, List) -> Maybe<Cons> = \ op : RpnOperation, listHead : List -> {
        switch (op : RpnOperation) {
            RpnBinaryOperation(): switch (listHead : List) {
                Cons(first, next): switch (next : List) {
                    Cons(second, listTail): switch (op : RpnBinaryOperation) {
                        RpnAdd(): Some(Cons(first + second, listTail));
                        RpnSubtract(): Some(Cons(second - first, listTail));
                        RpnMultiply(): Some(Cons(first * second, listTail));
                        RpnDivide(): {
                            if (first == 0.0) {
                                println("Dvivsion by zero");
                                None();
                            } else {
                                Some(Cons(second / first, listTail));
                            }
                        }
                        RpnPower(): {
                            if (second > 0.0) {
                                Some(Cons(exp(first * log(second)), listTail));
                            } else if (isInt_(first) && second != 0.0) {
                                result : double = exp(first * log(abs(second)));
                                Some(Cons(if (second < 0.0 && !even(round(second))) -result else result, listTail));
                            } else {
                                println(
                                    if (second == 0.0 && second == 0.0) {
                                        "Cannot evaluate \"" + d2s(second) + "**" + d2s(second) + "\""
                                    } else {
                                        "Cannot evaluate \"" + d2s(second) + "**" + d2s(second) 
                                            + "\": exponent is not integer (although it may seem so)"
                                    }
                                );
                                None();
                            }
                        }
                    }
                    EmptyList(): {
                        println("Not enough operands");
                        None();
                    }
                }
                EmptyList(): {
                    println("Not enough operands");
                    None();
                }
            };
            RpnUnaryOperation(): switch (listHead : List) {
                Cons(value, listTail): switch (op : RpnUnaryOperation) {
                    RpnLog(): {
                        if (value <= 0.0) {
                            println("Cannot apply log to negative value or 0");
                            None();
                        } else {
                            Some(Cons(log(value), listTail));
                        }
                    }
                    RpnNegative(): Some(Cons(-value, listTail)); 
                }
                EmptyList(): {
                    println("Not enough operands");
                    None();
                }
            };
        }
	}

    result : Maybe<List<double>> = fold(rpnExpression, Some(makeList()), \ list : Maybe<List>, token : RpnToken -> {
        maybeBind(list, \ listHead : List -> {
            switch (token : RpnToken) {
                RpnNumber(value): Some(Cons(value, listHead)); 
                RpnOperation(): applyOp(token, listHead);
                RpnVariable(name): {
                    switch (lookupTree(variableValues, name) : Maybe<double>) {
                        Some(value): Some(Cons(value, listHead));
                        None(): {
                            println("No value provided for variable \"" + name + "\"");
                            None();
                        }
                    }
                }
            }
        });
    });

    maybeBind(result, \ head : List<double> -> {
        switch (head : List<double>) {
            Cons(value, next): {
                if (next == EmptyList()) {
                    Some(value);
                } else {
                    println("Not enough operations to use all values");
                    None();
                }
            }
            EmptyList(): None();
        }
    });
}

isInt_(d : double) -> bool {
    i2d(round(d)) == d;
}
